import Headers from "./Component/header";
import "./App.css";
import Portfolio from "./Component/portfolioLogin";
import News from "./Component/news";
import React, { useState, useEffect } from "react";
import Coinvalue from "./Component/Coinvalue/App";
import "bootstrap/dist/css/bootstrap.min.css";
import HomePage from "./Component/HomePage";
import Login from "./Component/Login";
import Register from "./Component/Register";
import MyPortfolio from "./Component/myPortfolio";
// import CreatePortFolio from "./Component/Createportfolio/createPortfolio";
import firebase from "./Component/firebase";
import { Provider } from "./Context";
// import Footer from "./Component/footer";

import { BrowserRouter as Router, Route } from "react-router-dom";

export default function App() {
  const [firebaseInitialized, setFirebaseInitialized] = useState(false);

  useEffect(() => {
    firebase.isInitialized().then(val => {
      setFirebaseInitialized(val);
    });
  });

  return firebaseInitialized !== false ? (
    <div className="container">
      <Provider>
        <Router>
          <Route path="" component={Headers} />

          <Route path="/" exact component={Coinvalue}></Route>
          <Route
            path="/MarketCoinValue"
            exact
            render={() => {
              return <Coinvalue />;
            }}
          />
          <Route
            path="/news"
            exact
            render={() => {
              return <News />;
            }}
          />
          <Route path="/loginhome" component={HomePage} />
          <Route
            exact
            path="/login"
            render={() => {
              return <Login />;
            }}
          />
          <Route exact path="/register" component={Register} />

          <Route
            path="/portfoliologin"
            exact
            render={() => {
              return <Portfolio />;
            }}
          />

          <Route exact path="/portfolio" component={MyPortfolio} />
          {/* <Route path="" component={Footer} /> */}
        </Router>
      </Provider>
    </div>
  ) : (
    <div id="loader">Loading...</div>
  );
}
