import React, { Component } from 'react';
import firebase from "./Component/firebase"

export const Context = React.createContext();

export class Provider extends Component{
    state = {
        user : firebase.getCurrentUser()
    }

    getUser = (user) => {
        this.setState({
            user
        })
    }

    updateUser = ()=>{
    this.setState({user:null})
    }

    render() {
        return (
            <Context.Provider value={{user: this.state.user, getUser: this.getUser,updateUser:this.updateUser}}>
                {this.props.children}
            </Context.Provider>
        )
    }
}

export const Consumer = Context.Consumer;