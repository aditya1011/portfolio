import React, { Component } from 'react';

class Footer extends Component {
  state = {};
  render() {
    return (
      <div class="footer_main_menu">
        <button
          type="button"
          id="AsStoreExitView"
          class="orange-btn-sm orange-bg color-white exit-btn-view font-14 pull-right"
          style="display: none;"
        >
          Exit
        </button>{" "}
        <button
          type="button"
          id="ViewInventory"
          class="orange-btn-sm orange-bg color-white view-inventory-btn-view font-13 pull-right"
          style="padding: 10px; display: none;"
        >
          View Inventory
        </button>
        <div class="container main_container">
          <div class="footer-menu">
            <div class="row">
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 menu-col-view">
                <h3>My Account</h3>
                <ul>
                  <li>
                    <a
                      href="javascript:void(0);"
                      title="My Orders"
                      data-href="https://www.allensolly.com/myaccount/orders?source=footer"
                      class="myaccount-link load_click_ie"
                    >
                      Orders
                    </a>
                  </li>
                  <li>
                    <a
                      href="javascript:void(0);"
                      title="Returns / Refunds"
                      data-href="https://www.allensolly.com/myaccount/orders?source=footer"
                      class="myaccount-link load_click_ie"
                    >
                      Returns / Refunds
                    </a>
                  </li>
                  <li>
                    <a
                      title="Track Order"
                      href="javascript:void(0);"
                      data-toggle="tooltip"
                      id="track_order_icon"
                    >
                      Track Order
                    </a>
                  </li>
                  <li>
                    <a
                      title="Frequently Asked Questions"
                      href="https://www.allensolly.com/content/faqs-2?source=footer"
                    >
                      Frequently Asked Questions
                    </a>
                  </li>
                </ul>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <h3>Policies</h3>
                <ul>
                  <li>
                    <a
                      title="Payment Options"
                      href="https://www.allensolly.com/content/payment-options-5?source=footer"
                    >
                      Payment Options
                    </a>
                  </li>
                  <li>
                    <a
                      title="Terms and Conditions"
                      href="https://www.allensolly.com/content/terms-and-conditions-of-use-3?source=footer"
                    >
                      Terms &amp; Conditions of Use
                    </a>
                  </li>
                  <li>
                    <a
                      title="Terms &amp; Conditions of Membership Program"
                      href="https://www.allensolly.com/content/mysolly?source=footer"
                    >
                      Terms &amp; Conditions of Membership Program
                    </a>
                  </li>
                  <li>
                    <a
                      title="Offers Terms &amp; Conditions of Use"
                      href="https://www.allensolly.com/content/tc-11?source=footer"
                    >
                      Offer Terms &amp; Conditions{" "}
                    </a>
                  </li>
                  <li>
                    <a
                      title="Returns &amp; Exchange Policy"
                      href="https://www.allensolly.com/content/returns-exchange-policy-7?source=footer"
                    >
                      Returns &amp; Exchange Policy
                    </a>
                  </li>
                  <li>
                    <a
                      title="Shipping Policy"
                      href="https://www.allensolly.com/content/shipping-policy-1?source=footer"
                    >
                      Shipping Policy
                    </a>
                  </li>
                  <li>
                    <a
                      title="Privacy Policy"
                      href="https://www.allensolly.com/content/privacy-policy-13?source=footer"
                    >
                      Privacy Policy
                    </a>
                  </li>
                </ul>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 menu-col-view">
                <h3>Contact Us</h3>
                <ul>
                  <li>
                    <a
                      title="Customer Support"
                      href="https://www.allensolly.com/content/customer-support?source=footer"
                    >
                      Customer Support
                    </a>
                  </li>
                  <li>
                    <a
                      title="Store Locators"
                      href="https://www.allensolly.com/content/store-locators-9?source=footer"
                    >
                      Store Locators
                    </a>
                  </li>
                  <li>
                    <a
                      title="Help Center"
                      href="https://www.allensolly.com/helpcenter?source=footer"
                    >
                      Help Center
                    </a>
                  </li>
                </ul>
                <h3 class="as_aboutus">About us</h3>
                <ul>
                  <li>
                    <a
                      title="Official Brand Store"
                      href="https://www.allensolly.com/content/why-us-16?source=footer"
                    >
                      Official Brand Store
                    </a>
                  </li>
                  <li>
                    <a
                      title="About us"
                      href="https://www.allensolly.com/content/about-us-4?source=footer"
                    >
                      About Us
                    </a>
                  </li>
                  <li>
                    <a
                      href="http://www.abfrl.com/"
                      target="_blank"
                      rel="noopener"
                    >
                      Aditya Birla Fashion &amp; Retail Ltd
                    </a>
                  </li>
                </ul>
              </div>
              <div
                class="col-lg-4 col-md-4 col-sm-4 col-xs-4 top-cat-view TopCategories"
                style="padding-left: 10px;"
              >
                <h3>Top Categories</h3>
                <ul class="col-lg-3 col-md-3 col-sm-3 col-xs-4 menu-col-view">
                  <li>
                    <h4>MEN</h4>
                  </li>
                  <li>
                    <a
                      title="Shirts"
                      href="https://www.allensolly.com/category/men/shirts-5?source=footer&amp;page=1"
                      class="categoryLinks-VS"
                    >
                      Shirts
                    </a>
                  </li>
                  <li>
                    <a
                      title="T-shirts"
                      href="https://www.allensolly.com/category/men/t-shirts-6?source=footer&amp;page=1"
                      class="categoryLinks-VS"
                    >
                      T-Shirts
                    </a>
                  </li>
                  <li>
                    <a
                      title="Jackets"
                      href="https://www.allensolly.com/category/men/jackets-13?source=footer&amp;page=1"
                      class="categoryLinks-VS"
                    >
                      Jackets
                    </a>
                  </li>
                  <li>
                    <a
                      title="Footwear"
                      href="https://www.allensolly.com/category/men/trousers-chinos-7?source=footer&amp;page=1"
                      class="categoryLinks-VS"
                    >
                      Trousers
                    </a>
                  </li>
                  <li style="display: none;">
                    <a
                      title="Footwear"
                      href="https://www.allensolly.com/category/men/footwear-14?source=footer&amp;page=1"
                      class="categoryLinks-VS"
                    >
                      Footwear
                    </a>
                  </li>
                </ul>
                <ul class="col-lg-5 col-md-5 col-sm-5 col-xs-5 menu-col-view">
                  <li>
                    <h4>WOMEN</h4>
                  </li>
                  <li>
                    <a
                      title="Tees &amp; Tops"
                      href="https://www.allensolly.com/category/women/tees-tops-17?source=footer&amp;page=1"
                      class="categoryLinks-VS"
                    >
                      Tees &amp; Tops
                    </a>
                  </li>
                  <li>
                    <a
                      title="Dresses"
                      href="https://www.allensolly.com/category/women/dresses-18?source=footer&amp;page=1"
                      class="categoryLinks-VS"
                    >
                      Dresses
                    </a>
                  </li>
                  <li>
                    <a
                      title="Shirts and Blouses"
                      href="https://www.allensolly.com/category/women/shirts-blouses-16?source=footer&amp;page=1"
                      class="categoryLinks-VS"
                    >
                      Shirts and Blouses
                    </a>
                  </li>
                  <li>
                    <a
                      title="Jeans &amp; Jeggings"
                      href="https://www.allensolly.com/category/women/jeans-jeggings-22?source=footer&amp;page=1"
                      class="categoryLinks-VS"
                    >
                      Jeans &amp; Jeggings
                    </a>
                  </li>
                  <li style="display: none;">
                    <a
                      title="Footwear"
                      href="https://www.allensolly.com/category/women/footwear-146?source=footer&amp;page=1"
                      class="categoryLinks-VS"
                    >
                      Footwear
                    </a>
                  </li>
                </ul>
                <ul class="col-lg-4 col-md-4 col-sm-4 col-xs-3 menu-col-view pad-left-zero pad-right-zero">
                  <li>
                    <h4>JUNIORS</h4>
                  </li>
                  <li>
                    <a
                      title="Shirts &amp; Tees"
                      href="https://www.allensolly.com/category/boys/shirts-tees-90?source=footer&amp;page=1"
                      class="categoryLinks-VS"
                    >
                      Shirts &amp; Tees
                    </a>
                  </li>
                  <li>
                    <a
                      title="Frocks"
                      href="https://www.allensolly.com/category/girls/frocks-187?source=footer&amp;page=1"
                      class="categoryLinks-VS"
                    >
                      Frocks
                    </a>
                  </li>
                  <li>
                    <a
                      title="Tops, Tees and Blouses"
                      href="https://www.allensolly.com/category/girls/tops-tees-blouses-101?source=footer&amp;page=1"
                      class="categoryLinks-VS"
                    >
                      Tops, Tees and Blouses
                    </a>
                  </li>
                  <li>
                    <a
                      title="Bottoms"
                      href="https://www.allensolly.com/category/boys/bottoms-91?source=footer&amp;page=1"
                      class="categoryLinks-VS"
                    >
                      Bottoms
                    </a>
                  </li>
                </ul>
                <div class="clearfix"></div>
                <ul class="clearfix footer-social-icons">
                  <li class="pull-left">
                    <a
                      href="https://www.facebook.com/allensolly/"
                      target="_blank"
                      rel="noopener"
                    >
                      {" "}
                      <svg
                        xmlns="https://www.w3.org/2000/svg"
                        width="30"
                        height="30"
                        viewBox="0 0 90 90"
                      >
                        <path d="M90 15C90 7.1 82.9 0 75 0H15C7.1 0 0 7.1 0 15v60C0 82.9 7.1 90 15 90H45V56H34V41h11v-5.8C45 25.1 52.6 16 61.9 16H74v15H61.9C60.5 31 59 32.6 59 35V41h15v15H59v34h16c7.9 0 15-7.1 15-15V15z"></path>
                      </svg>{" "}
                    </a>
                  </li>
                  <li class="pull-left">
                    <a
                      href="https://twitter.com/allensolly"
                      target="_blank"
                      rel="noopener"
                    >
                      {" "}
                      <svg
                        xmlns="https://www.w3.org/2000/svg"
                        width="30"
                        height="30"
                        viewBox="0 0 612 612"
                      >
                        <path
                          d="M612 116.3c-22.5 10-46.7 16.8-72.1 19.8 25.9-15.5 45.8-40.2 55.2-69.4 -24.3 14.4-51.2 24.8-79.8 30.5 -22.9-24.4-55.5-39.7-91.6-39.7 -69.3 0-125.6 56.2-125.6 125.5 0 9.8 1.1 19.4 3.3 28.6C197.1 206.3 104.6 156.3 42.6 80.4c-10.8 18.5-17 40.1-17 63.1 0 43.6 22.2 82 55.8 104.5 -20.6-0.7-39.9-6.3-56.9-15.8v1.6c0 60.8 43.3 111.6 100.7 123.1 -10.5 2.8-21.6 4.4-33.1 4.4 -8.1 0-15.9-0.8-23.6-2.3 16 49.9 62.3 86.2 117.3 87.2 -42.9 33.7-97.1 53.7-155.9 53.7 -10.1 0-20.1-0.6-29.9-1.7 55.6 35.7 121.5 56.5 192.4 56.5 230.9 0 357.2-191.3 357.2-357.2l-0.4-16.3C573.9 163.5 595.2 141.4 612 116.3z"
                          fill="#010002"
                        ></path>
                      </svg>{" "}
                    </a>
                  </li>
                  <li class="pull-left">
                    <a
                      href="https://in.pinterest.com/allensollyindia/"
                      target="_blank"
                      rel="noopener"
                    >
                      {" "}
                      <svg
                        xmlns="https://www.w3.org/2000/svg"
                        width="30"
                        height="30"
                        viewBox="0 0 438.6 438.6"
                      >
                        <path d="M414.4 24.1C398.3 8 379 0 356.3 0V0H82.2C59.6 0 40.2 8 24.1 24.1 8.1 40.2 0 59.6 0 82.2v274.1c0 22.6 8 42 24.1 58.1 16.1 16.1 35.5 24.1 58.1 24.1h52.3c-3.2-29.3-2.6-53.5 2-72.5l28-118.2c-4.6-9.3-6.9-20.8-6.9-34.5 0-15.8 4-29.1 12.1-39.8 8.1-10.8 17.9-16.1 29.5-16.1 9.3 0 16.5 3.1 21.6 9.3 5 6.2 7.6 13.9 7.6 23.3 0 5.9-1 13-3.1 21.3 -2.1 8.3-4.9 18-8.4 29.1 -3.5 11.1-6 19.9-7.6 26.4 -2.7 11.4-0.5 21.3 6.6 29.6 7 8.3 16.3 12.4 27.7 12.4 20.2 0 36.7-11.2 49.5-33.7 12.8-22.5 19.3-49.7 19.3-81.6 0-24.6-7.9-44.5-23.8-60 -15.9-15.4-38-23.1-66.4-23.1 -31.8 0-57.5 10.1-77.2 30.4 -19.7 20.3-29.6 44.7-29.6 73.2 0 16.7 4.8 31 14.3 42.8 3.2 3.6 4.3 7.6 3.1 12 -1 3.2-2.4 9-4.3 17.4 -0.8 2.7-2.1 4.5-4.1 5.4 -2 1-4.2 1-6.7 0 -14.7-6.1-25.7-16.6-33.3-31.4 -7.5-14.8-11.3-32-11.3-51.4 0-12.6 2-25.2 6.1-37.8 4.1-12.7 10.4-24.8 19-36.5 8.6-11.7 18.8-22.1 30.7-31.3 11.9-9.1 26.5-16.4 43.7-21.8 17.2-5.4 35.7-8.1 55.5-8.1 20.2 0 38.9 3.5 56.3 10.4 17.3 6.9 31.8 16.2 43.5 27.7 11.7 11.5 20.9 24.7 27.5 39.7 6.7 14.9 10 30.4 10 46.4 0 43-10.9 78.6-32.7 106.6 -21.8 28.1-49.9 42.1-84.4 42.1 -11.4 0-22.1-2.7-32.1-8 -10-5.3-17-11.7-21-19.1 -8.4 33.3-13.4 53.2-15.1 59.7 -4.4 16.8-14.7 36.7-30.8 60H356.3c22.6 0 42-8 58.1-24.1 16.1-16.1 24.1-35.5 24.1-58.1v-274.1C438.5 59.6 430.5 40.2 414.4 24.1z"></path>
                      </svg>{" "}
                    </a>
                  </li>
                  <li class="pull-left">
                    <a
                      href="https://www.instagram.com/allensollyindia/"
                      target="_blank"
                      rel="noopener"
                    >
                      {" "}
                      <svg
                        xmlns="https://www.w3.org/2000/svg"
                        width="30"
                        height="30"
                        viewBox="0 0 97.4 97.4"
                      >
                        <path d="M12.5 0h72.4c6.9 0 12.5 5.1 12.5 12.5v72.4c0 7.4-5.6 12.5-12.5 12.5H12.5C5.6 97.4 0 92.3 0 84.9V12.5C0 5.1 5.6 0 12.5 0L12.5 0zM70.9 10.8c-2.4 0-4.4 2-4.4 4.4v10.5c0 2.4 2 4.4 4.4 4.4h11c2.4 0 4.4-2 4.4-4.4V15.2c0-2.4-2-4.4-4.4-4.4H70.9L70.9 10.8zM86.4 41.2h-8.6c0.8 2.6 1.3 5.5 1.3 8.4 0 16.2-13.6 29.3-30.3 29.3 -16.7 0-30.3-13.1-30.3-29.3 0-2.9 0.4-5.7 1.3-8.4h-8.9v41.1c0 2.1 1.7 3.9 3.9 3.9h67.8c2.1 0 3.9-1.7 3.9-3.9V41.2H86.4zM48.8 29.5c-10.8 0-19.6 8.5-19.6 19 0 10.5 8.8 19 19.6 19 10.8 0 19.6-8.5 19.6-19C68.4 38 59.6 29.5 48.8 29.5z"></path>
                      </svg>{" "}
                    </a>
                  </li>
                  <li class="pull-left">
                    <a
                      href="https://www.youtube.com/user/allensollyindia"
                      target="_blank"
                      rel="noopener"
                    >
                      {" "}
                      <svg
                        xmlns="https://www.w3.org/2000/svg"
                        width="30"
                        height="30"
                        viewBox="0 0 90 90"
                      >
                        <path d="M70.9 65.8H66l0-2.9c0-1.3 1-2.3 2.3-2.3h0.3c1.3 0 2.3 1 2.3 2.3L70.9 65.8zM52.4 59.7c-1.3 0-2.3 0.8-2.3 1.9V75.5c0 1 1 1.9 2.3 1.9 1.3 0 2.3-0.8 2.3-1.9V61.6C54.7 60.5 53.7 59.7 52.4 59.7zM82.5 51.9v26.5C82.5 84.8 77 90 70.2 90H19.8C13 90 7.5 84.8 7.5 78.4V51.9c0-6.4 5.5-11.6 12.3-11.6H70.2C77 40.3 82.5 45.5 82.5 51.9zM23.1 81.3l0-28 6.3 0v-4.1l-16.7 0v4.1l5.2 0v28H23.1zM41.9 57.5h-5.2v14.9c0 2.2 0.1 3.2 0 3.6 -0.4 1.2-2.3 2.4-3.1 0.1 -0.1-0.4 0-1.6 0-3.6l0-15h-5.2l0 14.8c0 2.3-0.1 4 0 4.7 0.1 1.4 0.1 2.9 1.3 3.8 2.3 1.7 6.8-0.3 8-2.7l0 3.1 4.2 0L41.9 57.5 41.9 57.5zM58.6 74.6L58.6 62.2c0-4.7-3.5-7.6-8.4-3.7l0-9.2 -5.2 0 0 31.9 4.3-0.1 0.4-2C55.1 84.1 58.6 80.6 58.6 74.6zM74.9 73l-3.9 0c0 0.2 0 0.3 0 0.5v2.2c0 1.2-1 2.1-2.1 2.1h-0.8c-1.2 0-2.1-1-2.1-2.1V75.5v-2.4 -3.1h9v-3.4c0-2.5-0.1-4.9-0.3-6.3 -0.6-4.5-6.9-5.2-10.1-2.9 -1 0.7-1.7 1.7-2.2 2.9 -0.4 1.3-0.7 3-0.7 5.3v7.4C61.7 85.3 76.7 83.6 74.9 73zM54.8 32.7c0.3 0.7 0.7 1.2 1.3 1.6 0.6 0.4 1.3 0.6 2.1 0.6 0.8 0 1.4-0.2 2-0.6 0.6-0.4 1.1-1 1.5-1.9l-0.1 2h5.8V9.7H62.8v19.2c0 1-0.9 1.9-1.9 1.9 -1 0-1.9-0.9-1.9-1.9V9.7h-4.8v16.7c0 2.1 0 3.5 0.1 4.3C54.4 31.4 54.6 32.1 54.8 32.7zM37.2 18.8c0-2.4 0.2-4.2 0.6-5.6 0.4-1.3 1.1-2.4 2.1-3.2 1-0.8 2.3-1.2 3.9-1.2 1.3 0 2.5 0.3 3.5 0.8 1 0.5 1.7 1.2 2.2 2 0.5 0.8 0.9 1.7 1.1 2.6 0.2 0.9 0.3 2.2 0.3 4v6.3c0 2.3-0.1 4-0.3 5.1 -0.2 1.1-0.6 2.1-1.1 3 -0.6 0.9-1.3 1.6-2.2 2.1 -0.9 0.4-2 0.7-3.2 0.7 -1.3 0-2.4-0.2-3.4-0.6 -0.9-0.4-1.6-1-2.1-1.7 -0.5-0.8-0.9-1.7-1.1-2.8 -0.2-1.1-0.3-2.7-0.3-4.9L37.2 18.8 37.2 18.8zM41.8 28.6c0 1.4 1 2.5 2.3 2.5 1.3 0 2.3-1.1 2.3-2.5V15.4c0-1.4-1-2.5-2.3-2.5 -1.3 0-2.3 1.1-2.3 2.5V28.6zM25.7 35.2h5.5l0-19 6.5-16.2h-6l-3.4 12.1L24.7 0h-5.9l6.9 16.3L25.7 35.2z"></path>
                      </svg>{" "}
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Footer;
