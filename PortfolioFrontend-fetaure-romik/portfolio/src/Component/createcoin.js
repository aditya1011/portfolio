import React, { Component } from "react";
import {
  Button,
  CardDeck,
  Card,
  Container,
  Row,
  Col,
  Table
} from "react-bootstrap";
import { MDBContainer, MDBRow, MDBCol } from "mdbreact";
import EmptyBox from "./images/emptybox.png";
import AddCoin from "./addCoin";
import { MdAdd } from "react-icons/md";
import { FaEdit } from "react-icons/fa";
import EditPortfolio from "./editPortfolio";

class UserPortfolio extends Component {
  constructor(props) {
    super(props);
    this.state = { addModalShow: false, showComponent:false};

    // this._onButtonClick = this._onButtonClick.bind(this);
  }
  render() {
    let addModalClose = () => this.setState({ addModalShow: false,showComponent:false });
    return (
      <Container >
        <Row style={{ backgroundColor: "#b5d0e0" }}>
          <Col>
            <h3
              style={{
                // marginLeft:"5px",
                fontFamily: "open sans condensed,sans-serif",
                fontSize: "15px",
                fontWeight: "700",
                color: "#396aab",
                lineHeight: "30px"
              }}
            >
              {" Portfolio Currency"}
            </h3>
          </Col>
          <Col
            md="auto"
            style={{
              textAlign: "right",              
              lineHeight: "30px"
            }}
          >
            <button            
              className="md-icon-button md-mini md-raised md-button md-ink-ripple "
              type="button"
              onClick={() => this.setState({ addModalShow: false,showComponent:true})}
              // tooltipbottom="Edit or delete"
              // tooltipplacement="bottom"
              // tooltiptrigger="mouseenter"
            >
              <FaEdit
                md-font-set="fa"
                md-font-icon="fa-edit"
                aria-label="Edit dialog"
                className=" fa fa-edit"
                aria-hidden="true"
              ></FaEdit>
              <div className="md-ripple-container"></div>
            </button>
            
            <EditPortfolio show={this.state.showComponent} onHide={addModalClose} />
            
          </Col>
          <Col
            xs
            lg="2"
            style={{
              textAlign: "right",
              // backgroundColor: "#b5d0e0",
              lineHeight: "30px"
            }}
          >
            <Button 
              variant="outline-light"
              style={{ backgroundColor: "#ff8e13",lineHeight: "20px" }}
              onClick={() => this.setState({ addModalShow: true })}
            >
              <MdAdd /> Coin
            </Button>
            {/* <AddCoin show={this.state.addModalShow} onHide={addModalClose} /> */}
          </Col>
        </Row>

        <CardDeck>
          <Card border="dark" style={{ fontSize: "14px", width: "3%" }}>
            <Card.Header style={{ backgroundColor: "#b5d0e0" }}>
              Acquistion Cost
            </Card.Header>
            <Card.Body>
              {/* <Card.Title>0000</Card.Title> */}
              <Card.Text>0.00</Card.Text>
            </Card.Body>
          </Card>
          <Card border="dark" style={{ width: "0.5em" }}>
            <Card.Header style={{ backgroundColor: "#b5d0e0" }}>
              Realised P/L
            </Card.Header>
            <Card.Body>
              {/* <Card.Title>0000</Card.Title> */}
              <Card.Text>0.00</Card.Text>
            </Card.Body>
          </Card>
          <Card border="dark" style={{ width: "0.5em" }}>
            <Card.Header style={{ backgroundColor: "#b5d0e0" }}>
              Profit / Loss
            </Card.Header>
            <Card.Body>
              {/* <Card.Title>0000</Card.Title> */}
              <Card.Text>0.00</Card.Text>
            </Card.Body>
          </Card>
          <Card border="dark" style={{ width: "0.5em" }}>
            <Card.Header style={{ backgroundColor: "#b5d0e0" }}>
              Holdings
            </Card.Header>
            <Card.Body>
              {/* <Card.Title>0000</Card.Title> */}
              <Card.Text>0.00</Card.Text>
            </Card.Body>
          </Card>
          <Card border="dark" style={{ width: "0.5em" }}>
            <Card.Header style={{ backgroundColor: "#b5d0e0" }}>
              24 Profit / Loss
            </Card.Header>
            <Card.Body>
              {/* <Card.Title>0000</Card.Title> */}
              <Card.Text>0.00</Card.Text>
            </Card.Body>
          </Card>
        </CardDeck>
        <Container style={{ backgroundColor: "#E6EFF5" }}>
          <Row style={{ backgroundColor: "#f4f9ff" }}>
            <Col md={4}>
              <Button variant="light">Current </Button>
            </Col>
            <Col md={{ span: 4, offset: 4 }} style={{ textAlign: "right" }}>
              <Button variant="light">Collapse</Button>
            </Col>
          </Row>
          <Table striped bordered hover size="sm">
            <thead>
              <tr>
                <th style={{ textAlign: "center" }}>#</th>
                <th>Coin/Date</th>
                <th style={{ textAlign: "center" }}>Price</th>
                <th style={{ textAlign: "right" }}>Total Value</th>
                <th style={{ textAlign: "right" }}>Profit / Loss</th>
                <th style={{ textAlign: "right" }}>Change</th>
              </tr>
            </thead>
          </Table>
          <MDBContainer style={{ backgroundColor: "white" }}>
            <MDBRow>
              <MDBCol
                style={{
                  textAlign: "center",
                  color: "#ff8e13",
                  fontSize: "25px",
                  fontFamily: "sans-serif"
                  // fontWeight: "bold"
                }}
              >
                <img
                  src={EmptyBox}
                  alt="empty-box"
                  className="img-thumbnail"
                  style={{ width: "200px" }}
                />
                <div className="emptyportfolio-text mt-2">
                  Your Profile seems to be empty
                </div>

                <Button
                  className="emptyButton mt-2"
                  variant="outline-light"
                  style={{
                    backgroundColor: "#2B5A99",
                    color: "white",
                    fontSize: "12px"
                  }}
                  onClick={() => this.setState({ addModalShow: true })}
                >
                  <MdAdd />
                  Add your first coin
                </Button>
                <AddCoin
                  show={this.state.addModalShow}
                  onHide={addModalClose}
                />
              </MDBCol>
            </MDBRow>
          </MDBContainer>

          <Row className="emptyfooter mt-2">
            <Col></Col>
            <Col style={{ textAlign: "right" }}>
              {" "}
              <Button
                variant="outline-light"
                style={{ backgroundColor: "#ff8e13" }}
                onClick={() => this.setState({ addModalShow: true })}
              >
                <MdAdd />
                Coin
              </Button>
              {/* <AddCoin show={this.state.addModalShow} onHide={addModalClose} /> */}
            </Col>
          </Row>
        </Container>
        <p className="emptyExplanation ">
          * The conversion is done through BTC or ETH (eg Crypto - BTC * BTC -
          FIAT, depending on the more liquid pair). It can also be done through
          inverting the more liquid pair (Eg BTC-ETH is 1/ETH-BTC) or it is a
          division operation though BTC or ETH (Eg. DASH - ETH is DASH - BTC /
          ETH - BTC)
        </p>
        <p className="emptyExplanation">
          ** Price spikes usually happen when a coin is only trading on one
          exchange or it has low liquidity. We consider a price spike anything
          that goes up more than 10 times in a day.
        </p>
        <p className="emptyExplanation">
          {" "}
          - Free while in Beta testing, will be part of the portfolio
          subscription once it gets out of Beta.
        </p>
      </Container>
    );
  }
}

export default UserPortfolio;
