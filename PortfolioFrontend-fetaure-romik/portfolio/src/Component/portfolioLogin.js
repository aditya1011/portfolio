import React, { Component } from "react";
import {Link} from 'react-router-dom';
import PortfolioImage from "./images/portfolio.png";
import "../App.css";
import logo from "./images/chart-bar-regular.svg";


class portfolioLogin extends Component {
  state = {};
  render() {
    return (
      <div className="container-portfolio mt-4">
        <div className="header">
          <h1
            style={{
              fontFamily: "open sans condensed,sans-serif",
              fontSize: "30px",
              fontWeight: "700"
            }}
          >
            {/* <span class="fa fa-bar-chart"> </span>  */}
            <img className="Headerlogo mr-2" src={logo} alt="Logo" width="40px" />
            {" MY PORTFOLIOS"}
          </h1>
        </div>
        <div className="conatainer mt-2">
          <div className="container-body">
            <div className="container-bodyImg">
              {" "}
              <img
                className="rounded mx-auto d-block "
                src={PortfolioImage}
                alt="Portfolio"
                width="650px"
              />
            </div>

            <div className="container-bodytext">
              <h3
                className="text mt-1"
                style={{ color: "#ff8e13", fontSize: "22px" }}
              >
                {" "}
                This is where it all started!
              </h3>
            </div>

            <div className="container-bodysmall">
              <p>
                At some point in the future when your grandkids visit you, you
                can either tell them:
              </p>
              <div className="emptyportfolio ">
                <span
                  className="emptyportfolio-itemnumber ml-2 "
                  style={{
                    color: "#ff8e13",
                    fontSize: "14px",
                    fontWeight: "bold"
                  }}
                >
                  1
                </span>

                <span
                  className="emptyportfolio-item ml-2"
                  style={{
                    fontSize: "14px",
                    color: "#666666",
                    fontWeight: "bold"
                  }}
                >
                  You missed the boat on cryptocurrencies.
                </span>

                <div className="emptyportfolio">
                  <span
                    className="emptyportfolio-itemnumber ml-2"
                    style={{
                      color: "#ff8e13",
                      fontSize: "14px",
                      fontWeight: "bold"
                    }}
                  >
                    2
                  </span>

                  <span
                    className="emptyportfolio-item ml-2"
                    style={{
                      fontSize: "14px",
                      color: "#666666",
                      fontWeight: "bold"
                    }}
                  >
                    You were an early adopter and made money out of trading
                    cryptos!
                  </span>
                </div>
              </div>
              <div className="emptyportfolio-text mt-2">
                <span
                  className="emptyportfolio-textitem ml-2"
                  style={{ fontSize: "18px" }}
                >
                  {" But, before you choose option"}

                  <span
                    className="emptyportfolio-itemnumber ml-2"
                    style={{
                      color: "#ff8e13",
                      fontSize: "14px",
                      fontWeight: "bold"
                    }}
                  >
                    2
                  </span>
                  {"and join the crypto world, you need to"}
                </span>
              </div>
              <div className="emptyportfolio-footer mt-3">
                <div className="portfolio-button">
                  <Link to="/loginhome">
                  <button
                    type="button"
                    class="btn btn-md btn-warning"
                    style={{
                      color: "white"
                    }}
                  >
                    Login Or Register
                  </button>
                  </Link>
                
                </div>

                <p
                  className="portfolio-caution font-italic mt-2"
                  style={{ fontSize: "14px" }}
                >
                  Never invest in cryptos more than you can afford to lose and
                  always try to keep them in your own wallet!
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default portfolioLogin;
