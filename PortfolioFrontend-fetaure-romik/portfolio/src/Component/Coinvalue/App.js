import React from "react";
import Coinvalue from "./coinvalue";
import Charts from "./charts";

import "./Apps.css";

function App() {
  return (
    <div class="container">
      <Charts />
      <Coinvalue />
      
    </div>
  );
}

export default App;
