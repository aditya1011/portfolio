import React, { Component } from "react";
import Symbols from "./Symbols";
import { Tab, Tabs ,Table} from "react-bootstrap";
import News from "../news";

class Coinvalue extends Component {
  state = {
    news: [],
    heading: [
      "#",
      "Coin",
      "Price",
      "DirectVol",
      "TotalVol",
      "TopTierVol",
      "Mkt.Cap"
    ]
  };

  fetchFunction = () => {
    fetch(
      `https://min-api.cryptocompare.com/data/pricemultifull?fsyms=BTC,ETH,XRP,EOS,LTC,BNB,BCH,TRX,ETC,LINK&tsyms=USD,EUR&api_key={348c85ff1502da434c309cbf779d4ac3d266e5a265aae90953b66a0dd14b673f}`,
      {
        method: "GET"
      }
    )
      .then(res => res.json())
      .then(data => {
        console.log(data);
        console.log(data);
        let symbols = Object.values(data.DISPLAY);

        console.log(symbols);
        this.setState({ news: symbols });
        console.log(this.state.news);
      });
  };

  componentDidMount() {
    //   fetch(`https://min-api.cryptocompare.com/data/pricemultifull?fsyms=BTC,ETH,XRP,EOS,LTC,BNB,BCH,TRX,ETC,LINK&tsyms=USD,EUR&api_key={348c85ff1502da434c309cbf779d4ac3d266e5a265aae90953b66a0dd14b673f}`,
    //     {
    //         method:"GET"
    //     }).then((res)=>res.json())
    //    .then((data)=>
    //    {
    //        console.log(data)
    //      //  console.log(data.DISPLAY)
    //        let symbols=Object.values(data.DISPLAY)
    //        //console.log(symbols)
    //         console.log(symbols)
    //       this.setState({news:symbols})
    //        console.log(this.state.news)

    //         window.setInterval(this.componentDidMount,4000)
    //    })
    //   setInterval(this.fetchFunction,6000)
    this.fetchFunction();
  }

  render() {
    return (
      <Tabs
        defaultActiveKey="home"
        justified
        style={{
          borderColor: "#00d665",
          marginTop: "5%",
          fontFamily: "sans-serif",
          fontWeight: "bold"
        }}>

        <Tab eventKey="home" title="Coin" style={{}}>
          <div className="main">
            {/* <div className="coinHeader"> */}
              {/* {this.state.heading.map((value, index) => {
                //   console.log(value);
                return <span className="orderNumber">{value}</span>;
              })}
            </div> */}

<Table striped bordered hover>
  <thead>
<tr style={{textAlign:"center" ,color:"black",fontSize:"14px"}}>
{this.state.heading.map((value, index) => {
    console.log(value);
   
  return <th className="orderNumber">{value}</th>;
  
})}
</tr>
</thead>

            {this.state.news.map((values, index) => {
              console.log(index);
              return (
                <Symbols
                  ordernumber={index + 1}
                  price={values.USD.PRICE}
                  DirectVol={values.USD.VOLUME24HOURTO}
                  TotalVal={values.USD.TOTALVOLUME24HTO}
                  TopTierVal={values.USD.TOTALTOPTIERVOLUME24HTO}
                  MktCap={values.USD.MKTCAP}
                  FromSymbol={values.USD.FROMSYMBOL}
                  index={index}
                />
              );
            })}
             </Table>
          {/* </div> */}
          </div>
         
        </Tab>
        <Tab eventKey="News" title="News">
          <News />
        </Tab>
      </Tabs>
    );
  }
}

export default Coinvalue;
