import React, { Component } from "react";
import {
  AreaChart,
  Area,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip
} from "recharts";

export default class BitCoinChart extends Component {
  static jsfiddleUrl = "https://jsfiddle.net/alidingling/Lrffmzfc/";
  constructor(props) {
    super(props);
    this.state = {
      hourInformation: []
    };
    fetch(
      "https://min-api.cryptocompare.com/data/v2/histohour?limit=12&fsym=BTC&tsym=USD&key=348c85ff1502da434c309cbf779d4ac3d266e5a265aae90953b66a0dd14b673f",
      {
        method: "GET"
      }
    )
      .then(res => res.json())
      .then(data => {
        //this.setState({ hourInformation: data.Data.Data })
        let hour = data.Data.Data.reduce((accumulator, currentvalue) => {
          var utcSeconds = currentvalue.time;
          var d = new Date(0); // The 0 there is the key, which sets the date to the epoch
          d.setUTCSeconds(utcSeconds);
          d = d.getHours() + ":" + d.getMinutes();
          let obj = {};
          obj["time"] = d;
          obj["high"] = currentvalue.high;
          accumulator.push(obj);
          return accumulator;
        }, []);
        this.setState({ hourInformation: hour });
        console.log("constructor", this.state.hourInformation);
      });
  }

  render() {
    return (
      <AreaChart
        width={275}
        height={150}
        data={this.state.hourInformation}
        margin={{
          top: 10,
          right: 30,
          left: 0,
          bottom: 0
        }}
      >
        <CartesianGrid strokeDasharray="8  8" />
        <XAxis dataKey="time" style={{ fontSize: "14px" ,color:"green" }}></XAxis>
        <YAxis
          type="number"
          style={{ fontSize: "14px" }}
          domain={[8000, 8090]}
        ></YAxis>
        <Tooltip />
        <Area type="sans-serif" dataKey="high" stroke="black" fill="#8884d8" fontSize="14px" />
      </AreaChart>
    );
  }
}
