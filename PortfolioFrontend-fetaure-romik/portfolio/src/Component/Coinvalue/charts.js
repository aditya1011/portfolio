import React, { Component } from 'react';
import Bitcoin from "./BitCoinChart";
import EtheriumChart from './EtheriumChart';
import LiteCoinChart from './LiteCoinChart';
import XrpChart from "./XrpChart";
import {Container,Row} from "react-bootstrap";


class Chart extends Component{
    render()
    {
        return (
        //    <div className="coinHistory mt-4">
            <Container mt-4><Row>
        <div class="coin">
        <h4>Bitcoin</h4>
        <Bitcoin/>
        </div>
        <div class="coin">
        <h4>Etherium</h4>
        <EtheriumChart/>
        </div>
        <div class="coin">
        <h4>LiteCoin</h4>
        <LiteCoinChart/>
        </div>
        <div class="coin">
        <h4>XRP<br/>    
        </h4>
        <XrpChart/>
        </div>
        </Row>
        </Container>);
        
    }
}

export  default Chart;
