import app from 'firebase/app'
import 'firebase/auth'
import 'firebase/firebase-firestore'

const config = {
	apiKey: "AIzaSyBdU8Axa-hBAc2iD1710oqpcvpqSt9bqWo",
	authDomain: "crypto-portfolio-d54e1.firebaseapp.com",
	databaseURL: "https://crypto-portfolio-d54e1.firebaseio.com",
	projectId: "crypto-portfolio-d54e1",
	storageBucket: "crypto-portfolio-d54e1.appspot.com",
	messagingSenderId: "456846139556",
	appId: "1:456846139556:web:611deac1fc66c1d5380631",
	measurementId: "G-YYVVZMPN1Q"
}

class Firebase {
	constructor() {
		app.initializeApp(config)
		this.auth = app.auth()
		this.db = app.firestore()
	}

	login(email, password) {
		return this.auth.signInWithEmailAndPassword(email, password)
	}

	logout() {
		
		return this.auth.signOut()
	}

	async register(name, email, password) {
		await this.auth.createUserWithEmailAndPassword(email, password)
		return this.auth.currentUser.updateProfile({
			displayName: name
		})
	}

	addPortfolio(userId,name,currency,desc) {
		return this.db.collection("portfolio").add({
			userId:userId,
			name: name,
			currency: currency,
			description:desc
		}).then(function(docRef) {
			console.log("Document written with ID: ", docRef.id);
		})
		.catch(function(error) {
			console.error("Error adding document: ", error);
		});
	}
   
	addQuote(quote) {
		if(!this.auth.currentUser) {
			return alert('Not authorized')
		}

		return this.db.doc(`users_codedamn_video/${this.auth.currentUser.uid}`).set({
			quote
		})
	}

	isInitialized() {
		return new Promise(resolve => {
			this.auth.onAuthStateChanged(resolve)
		})
	}

	getCurrentUser() {
		return this.auth.currentUser;
	}

	isLoggedin() {
		if (this.auth.currentUser) {
			return true;
		}
	}

	getCurrentUsername() {
		return this.auth.currentUser && this.auth.currentUser.displayName
	}

	async getCurrentUserQuote() {
		const quote = await this.db.doc(`users_codedamn_video/${this.auth.currentUser.uid}`).get()
		return quote.get('quote')
	}
}

export default new Firebase()