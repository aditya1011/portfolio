import React, { Component } from "react";


export default class NewsPage extends Component {
  state = {
    upvote: 0,
    downvote: 0
  };

  upvoteHandler() {
    if (this.state.upvote === 0) {
      this.setState({
        upvote: 1,
        downvote: 0
      });
    }
  }
  downvoteHandler() {
    if (this.state.upvote === 1) {
      this.setState({
        upvote: 0,
        downvote: 1
      });
      console.log("this is download handler");
    }
  }
  // getBadgeClasses() {
  //   let Classes = "badge m-2 badge-";
  //   Classes += this.state.count === 84 ? "primary" : "warning";
  //   return Classes;
  // }
  render() {
    let { id, source, title, body, imageurl, url, categories } = this.props;
    return (
      <div id={id} className="news">
        <img src={imageurl} alt="newsImage" height="100px" width="100px" />
        <div className="newsContent">
          <h4>{source}</h4>
          <a href={url}>
            <h3>{title}</h3>
          </a>
          <p>{body}</p>
          <footer className="footer">
            <div className="categories">
              Categories:<span className="newstag">{categories}</span>
            </div>
            <div className="footerButoon">
              <div className="upvote">
                <button
                
                  name="upvoteHandler"
                  onClick={() => {
                    this.upvoteHandler();
                  }}
                  style={{border:"0",color:"#337ab7",backgroundColor:"white"}}
                >
                  {"Upvote "}
                  
                </button>
                {/* <input
                  type="text"
                  value={this.state.upvote}
                  className="textBox2"
                  size="5"
                ></input> */}
                <span className= "badge m-2 badge-primary">{this.state.upvote}</span>
              </div>
              <div className="downvote">
                <button

                  type="button"
                  onClick={() => {
                    this.downvoteHandler();
                  }}
                  style={{border:"0",color:"#337ab7",backgroundColor:"white"}}
                >
                 {" Downvote"}
                  </button>
                
                {/* <input
                  type="text"
                  value={this.state.downvote}
                  className="textBox2"
                  size="5"
                ></input> */}
                <span className= "badge m-2 badge-danger">{this.state.downvote}</span>
              </div>
            </div>
          </footer>
        </div>
      </div>
    );
  }
}
