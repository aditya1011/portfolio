import React, { useState } from "react";
import { Modal, Button, Col, Form } from "react-bootstrap";
import firebase from "./firebase";
import { Consumer } from "../Context";

// import { withAlert } from 'react-alert'

function AddPortfolio(props, { alert }) {
  const [name, setName] = useState("");
  const [currency, setCurrency] = useState("");
  const [desc, setDesc] = useState("");
  const [userId, setUserId] = useState(null);

  let handleCreate = e => {
    e.preventDefault();
    if ({ name } === "" && { currency } === "") {
      return alert("Please Fill all required field");
    } else {
      firebase.addPortfolio({ userId }, { name }, { currency }, { desc });
    }
  };

  return (
    <Consumer>
      {value => {
        if (value.user) {
          setUserId(value.user.uid);
        }
        return (
          <Modal
            {...props}
            // size="md"
            aria-labelledby="contained-modal-title-vcenter"
            centered
          >
            <Modal.Header closeButton style={{ backgroundColor: "#ff8e13" }}>
              <Modal.Title
                id="contained-modal-title-vcenter"
                style={{
                  color: "white",
                  fontFamily: "sans-serif",
                  fontSize: "22px"
                }}
              >
                Edit a portfolio
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form onSubmit={handleCreate}>
                <Form.Row>
                  <Form.Group as={Col} controlId="formGridEmail">
                    <Form.Label>Name*</Form.Label>
                    <Form.Control
                      type="name"
                      placeholder="Enter Name"
                      onChange={e => setName(e.target.value)}
                      required="true"
                    />
                  </Form.Group>

                  <Form.Group as={Col} controlId="formGridState">
                    <Form.Label>Currency*</Form.Label>
                    <Form.Control
                      as="select"
                      onChange={e => setCurrency(e.target.value)}
                      required="true"
                    >
                      <option>USD</option>
                      <option>BTC</option>
                      <option>GBP</option>
                      <option>ETH</option>
                      <option>XRP</option>
                      <option>LTC</option>
                      <option>INR</option>
                      <option>GOLD</option>
                      <option>VEF</option>
                    </Form.Control>
                  </Form.Group>
                </Form.Row>
                <Form.Group controlId="formGridAddress1">
                  <Form.Label>Description</Form.Label>
                  <Form.Control
                    placeholder="Description..."
                    onChange={e => setDesc(e.target.value)}
                  />
                </Form.Group>

                <Modal.Footer>
                  {{ name } === "" && { currency } === "" ? (
                    <Button
                      type="submit"
                      className="btn btn-primary"
                      onClick={() => {
                        alert.show("Oh look, an alert!");
                      }}
                    >
                      {" "}
                      Create{" "}
                    </Button>
                  ) : (
                    <Button
                      type="submit"
                      className="btn btn-primary"
                      onClick={props.onHide}
                    >
                      {" "}
                      Create{" "}
                    </Button>
                  )}
                </Modal.Footer>
              </Form>
            </Modal.Body>
          </Modal>
        );
      }}
    </Consumer>
  );
}

export default AddPortfolio;
