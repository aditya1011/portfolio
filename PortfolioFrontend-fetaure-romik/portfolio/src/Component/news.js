import React, { Component } from "react";
import NewsPage from "./newsPage";

export default class News extends Component {
  state = {
    newsData: []
  };
  newsExtraction() {
    fetch("https://min-api.cryptocompare.com/data/v2/news/?lang=EN", {
      method: "GET"
    })
      .then(res => res.json())
      .then(data => {
        this.setState({ newsData: data.Data });
      })
      .then(() => {
        console.log("this is state news data", this.state.newsData);
      });
  }

  componentDidMount() {
    this.newsExtraction();
  }

  render() {
    return (
      <div className="newsContainer mt-4">
        {this.state.newsData.map(newsData => {
          return (
            <NewsPage
              id={newsData.id}
              url={newsData.url}
              source={newsData.source}
              title={newsData.title}
              body={newsData.body}
              imageurl={newsData.imageurl}
              categories={newsData.categories}
            />
          );
        })}
      </div>
    );
  }
}
