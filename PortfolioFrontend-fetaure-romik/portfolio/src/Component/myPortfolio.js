import React, { Component } from "react";
import AddPortFolio from "./addPortfolio";
import UserPortfolio from "./createcoin";
// import UserPortfolio from "./usersCoin";
import { Button, Container, Row, Col, Tab, Tabs } from "react-bootstrap";
import { FaRegChartBar } from "react-icons/fa";
import { MdAdd } from "react-icons/md";

class MyPortfolio extends Component {
  constructor(props) {
    super(props);
    this.state = { addModalShow: false, showComponent: false };

    // this._onButtonClick = this._onButtonClick.bind(this);
  }
  // _onButtonClick() {
  //   this.setState({
  //     showComponent: true
  //   });
  // }

  render() {
    let addModalClose = () => this.setState({ addModalShow: false });
    return (
      //"#f4f9ff"

      <Container style={{ backgroundColor: "white", border: "2px" }}>
        <Row style={{ backgroundColor: "white" }}>
          <Col md={4}>
            <h1
              style={{
                fontFamily: "open sans condensed,sans-serif",
                fontSize: "30px",
                fontWeight: "bold",
                lineHeight: "40px"
              }}
            >
              <FaRegChartBar
                className="barchart ml-2 mt-2"
                style={{ height: "20px", width: "30px" }}
              />
              {" MY PORTFOLIOS"}
            </h1>
          </Col>
          <Col style={{ textAlign: "right" }} md={{ span: 4, offset: 4 }}>
            <Button
              variant="outline-light"
              style={{ backgroundColor: "#ff8e13" }}
              onClick={() => this.setState({ addModalShow: true })}
            >
              <MdAdd className="add" /> PortFolio
            </Button>
            <AddPortFolio
              show={this.state.addModalShow}
              onHide={addModalClose}
            />
          </Col>
        </Row>
        {/* <div>
          <Button variant="light" onClick={this._onButtonClick}>
            UserPortfolio1
          </Button>
          {this.state.showComponent ? <UserPortfolio /> : null}
         
        </div> */}
        <Container>
          <Tabs defaultActiveKey="home" id="uncontrolled-tab-example" onClick={this._onButtonClick}>
            <Tab eventKey="home" title="Aditya"  >
              <UserPortfolio />
            </Tab>
            <Tab eventKey="Romik" title="Romik">
              <UserPortfolio />
            </Tab>
            <Tab eventKey="Laukesh" title="Romik">
              <UserPortfolio />
            </Tab>
          </Tabs>
        </Container>
      </Container>
    );
  }
}

export default MyPortfolio;
