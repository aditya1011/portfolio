import React, { Component } from "react";
import {
  Button,
  CardDeck,
  Card,
  Container,
  Row,
  Col,
  Table,Tab,Tabs
} from "react-bootstrap";
// import { MDBContainer, MDBRow, MDBCol } from "mdbreact";
// import EmptyBox from "./images/emptybox.png";
import AddCoin from "./addCoin";
import { MdAdd } from "react-icons/md";
import { FaEdit } from "react-icons/fa";

class UserPortfolio extends Component {
  state = { addModalShow: false };

  render() {
    let addModalClose = () => this.setState({ addModalShow: false });
    return (
      <Container >
        <Row style={{ backgroundColor: "#b5d0e0" }}>
          <Col>
            <h3
              style={{
                // marginLeft:"5px",
                fontFamily: "open sans condensed,sans-serif",
                fontSize: "30px",
                fontWeight: "700",
                color: "#396aab",
                lineHeight: "50px"
              }}
            >
              {" Portfolio Currency"}
            </h3>
          </Col>
          <Col
            md="auto"
            style={{
              textAlign: "right",              
              lineHeight: "50px"
            }}
          >
            <button
            
              class="md-icon-button md-mini md-raised md-button md-ink-ripple "
              type="button"
              onClick={() => this.setState({ addModalShow: true })}
              TooltipBottom="Edit or delete"
              tooltipPlacement="bottom"
              tooltipTrigger="mouseenter"
            >
              <FaEdit
                md-font-set="fa"
                md-font-icon="fa-edit"
                aria-label="Edit dialog"
                class=" fa fa-edit"
                aria-hidden="true"
              ></FaEdit>
              <div class="md-ripple-container"></div>
            </button>
            <AddCoin show={this.state.addModalShow} onHide={addModalClose} />
          </Col>
          <Col
            xs
            lg="2"
            style={{
              textAlign: "right",
              // backgroundColor: "#b5d0e0",
              lineHeight: "50px"
            }}
          >
            <Button
              variant="outline-light"
              style={{ backgroundColor: "#ff8e13" }}
              onClick={() => this.setState({ addModalShow: true })}
            >
              <MdAdd /> Coin
            </Button>
            <AddCoin show={this.state.addModalShow} onHide={addModalClose} />
          </Col>
        </Row>

        <CardDeck>
          <Card border="dark" style={{ fontSize: "14px", width: "3%" }}>
            <Card.Header style={{ backgroundColor: "#b5d0e0" }}>
              Acquistion Cost
            </Card.Header>
            <Card.Body>
              {/* <Card.Title>0000</Card.Title> */}
              <Card.Text>0.00</Card.Text>
            </Card.Body>
          </Card>
          <Card border="dark" style={{ width: "0.5em" }}>
            <Card.Header style={{ backgroundColor: "#b5d0e0" }}>
              Realised P/L
            </Card.Header>
            <Card.Body>
              {/* <Card.Title>0000</Card.Title> */}
              <Card.Text>0.00</Card.Text>
            </Card.Body>
          </Card>
          <Card border="dark" style={{ width: "0.5em" }}>
            <Card.Header style={{ backgroundColor: "#b5d0e0" }}>
              Profit / Loss
            </Card.Header>
            <Card.Body>
              {/* <Card.Title>0000</Card.Title> */}
              <Card.Text>0.00</Card.Text>
            </Card.Body>
          </Card>
          <Card border="dark" style={{ width: "0.5em" }}>
            <Card.Header style={{ backgroundColor: "#b5d0e0" }}>
              Holdings
            </Card.Header>
            <Card.Body>
              {/* <Card.Title>0000</Card.Title> */}
              <Card.Text>0.00</Card.Text>
            </Card.Body>
          </Card>
          <Card border="dark" style={{ width: "0.5em" }}>
            <Card.Header style={{ backgroundColor: "#b5d0e0" }}>
              24 Profit / Loss
            </Card.Header>
            <Card.Body>
              {/* <Card.Title>0000</Card.Title> */}
              <Card.Text>0.00</Card.Text>
            </Card.Body>
          </Card>
        </CardDeck>
        <Container style={{ backgroundColor: "#E6EFF5" }}>
          {/* <Row style={{ backgroundColor: "#f4f9ff" }}>
            <Col md={4}>
              <Button variant="light">Current </Button>
            </Col>
            <Col md={{ span: 4, offset: 4 }} style={{ textAlign: "right" }}>
              <Button variant="light">Collapse</Button>
            </Col>
          </Row> */}
          <Tabs defaultActiveKey="home" id="uncontrolled-tab-example">
  <Tab eventKey="home" title="Current">
          <Table striped bordered hover size="sm">
            <thead>
              <tr>
                <th style={{ textAlign: "center" }}>#</th>
                <th>Coin/Date</th>
                <th style={{ textAlign: "center" }}>Price</th>
                <th style={{ textAlign: "right" }}>Total Value</th>
                <th style={{ textAlign: "right" }}>Profit / Loss</th>
                <th style={{ textAlign: "right" }}>Change</th>
              </tr>
            </thead>
            <tbody>
            <tr>
      <td>
          BTC
      </td>
      <td>$1000</td>
      <td>$800</td>
      <td>$456.89</td>
      <td>$800</td>
      <td>$45</td>
    </tr></tbody>
          </Table>         

          <Row className="emptyfooter mt-2">
            <Col></Col>
            <Col style={{ textAlign: "right" }}>
              {" "}
              <Button
                variant="outline-light"
                style={{ backgroundColor: "#ff8e13" }}
                onClick={() => this.setState({ addModalShow: true })}
              >
                <MdAdd />
                Coin
              </Button>
              <AddCoin show={this.state.addModalShow} onHide={addModalClose} />
            </Col>
          </Row>
          </Tab>
          {/* <Tab eventKey="profile" title="Profile">
    <span>aditya</span>
        </Tab>*/}
  </Tabs>
        </Container>
        <p className="emptyExplanation ">
          * The conversion is done through BTC or ETH (eg Crypto - BTC * BTC -
          FIAT, depending on the more liquid pair). It can also be done through
          inverting the more liquid pair (Eg BTC-ETH is 1/ETH-BTC) or it is a
          division operation though BTC or ETH (Eg. DASH - ETH is DASH - BTC /
          ETH - BTC)
        </p>
        <p className="emptyExplanation">
          ** Price spikes usually happen when a coin is only trading on one
          exchange or it has low liquidity. We consider a price spike anything
          that goes up more than 10 times in a day.
        </p>
        <p className="emptyExplanation">
          {" "}
          - Free while in Beta testing, will be part of the portfolio
          subscription once it gets out of Beta.
        </p>
      </Container>
    );
  }
}

export default UserPortfolio;
