import React, { Component } from "react";
import logo from "./images/images.png";
import { NavLink } from "react-router-dom";
import { Dropdown } from "react-bootstrap";
import "../App.css";
import firebase from "./firebase";
import { Consumer, Context } from "../Context";

class Header extends Component {
  static contextType = Context;
  LogOut = () => {
    firebase.logout().then(() => {
      this.context.updateUser();
      this.props.history.replace("/portfoliologin");
      console.log(this.props);
    });
  };

  render() {
    return (
      <nav className="navbar navbar-expand-sm bg-light navbar-light">
        <NavLink to="/" className="navbar-brand">
          <img className="Header-logo" src={logo} alt="Logo" width="400px" />
        </NavLink>
        <div>
          <ul className="navbar-nav ml-auto ">
            <NavLink
              to="/MarketCoinValue"
              className="nav-link"
              style={{ color: "black" }}
            >
              <li className="nav-item mx-sm-4 my-sm-2">Market</li>
            </NavLink>

            <NavLink to="/news" className="nav-link" style={{ color: "black" }}>
              <li className="nav-item mx-sm-4 my-sm-2">News</li>
            </NavLink>
          </ul>
        </div>
        <div className="navbar-collapse collapse w-100 order-3 dual-collapse2">
          <ul className="navbar-nav ml-auto">
            <Consumer>
              {value => {
                return value.user ? (
                  <ul className="navbar-nav ml-auto">
                    <NavLink to="/portfolio" className="nav-link">
                      <li className="nav-item mr-4">Portfolio</li>
                    </NavLink>
                    <Dropdown>
                      <Dropdown.Toggle
                        variant="outline-primary"
                        id="dropdown-basic"
                      >
                        {firebase.getCurrentUsername()}
                      </Dropdown.Toggle>

                      <Dropdown.Menu>
                        <Dropdown.Item>
                          <button
                            onClick={this.LogOut}
                            className="btn btn-outline-danger btn-sm"
                          >
                            Log Out
                          </button>
                        </Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                  </ul>
                ) : (
                  <ul className="navbar-nav ml-auto">
                    <NavLink to="/portfoliologin" className="nav-link">
                      <li className="nav-item mr-4">Portfolio</li>
                    </NavLink>
                    <NavLink to="/loginhome" className="nav-link">
                      <li className="nav-item mr-4">Log in/Sign Up</li>
                    </NavLink>
                  </ul>
                );
              }}
            </Consumer>
          </ul>
        </div>
      </nav>
    );
  }
}

export default Header;
