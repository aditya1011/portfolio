import React, { Component } from "react";
import { Modal, Button, Col, Form } from "react-bootstrap";
import {MdMenu} from "react-icons/md";
class AddCoin extends Component {
  // constructor(props) {
  //   super(props);
  //   // this.state = {currency= ['USD','BTC','EUR']}
  // }
  render() {
    return (
      <Modal
        {...this.props}
        // size="md"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton style={{ backgroundColor: "#ff8e13" }}>
          <Modal.Title
            id="contained-modal-title-vcenter"
            style={{
              color: "white",
              fontFamily: "sans-serif",
              fontSize: "22px"
            }}
          >
            Add a new coin
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Row>
            <Form.Label placeholder="type or select coin"></Form.Label>
                  <Form.Control as="select">
                    <option>Bitcoin (BTC)</option>
                    <option>Ethereum(ETH)</option>
                    <option>Litecoin(LTC)</option>
                    <option>Dash(DASH)</option>
                    <option>Monero(XMR)</option>
                    <option>Nxt(NXT)</option>
                    <option>DogeCoin(DOGE)</option>
                    <option>Zcash(ZEC)</option>
                    <option>BitShares(BIT)</option>
                  </Form.Control>


            </Form.Row>
            <Form.Row>
              <Col>
                <Form.Label><MdMenu/>Amount*</Form.Label>
                <Form.Control placeholder="Amount" />
              </Col>
              <Col>
                <Form.Label>Buy Price*</Form.Label>
                <Form.Control placeholder="Buy price" />
              </Col>
            </Form.Row>
            <Form.Row>
              <Col>
                {/* <Form.Group as={Col} controlId="formGridState"> */}
                  <Form.Label>Currency*</Form.Label>
                  <Form.Control as="select">
                    <option>USD</option>
                    <option>BTC</option>
                    <option>GBP</option>
                    <option>ETH</option>
                    <option>XRP</option>
                    <option>LTC</option>
                    <option>INR</option>
                    <option>GOLD</option>
                    <option>VEF</option>
                  </Form.Control>
                {/* </Form.Group> */}
              </Col>
              <Col>
                {/* <Form.Group controlId="DOJ"> */}
                  <Form.Label>Date</Form.Label>
                  <Form.Control type="date" required placeholder="DOJ" />
                {/* </Form.Group> */}
              </Col>
            </Form.Row>

            <Form.Row>
              {/* <Form.Group controlId="formGridDescription"> */}
                <Form.Label>Description</Form.Label>
                <Form.Control placeholder="Description....." />
              {/* </Form.Group> */}
            </Form.Row>

          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button className="btn btn-danger" onClick={this.props.onHide}>
            Close
          </Button>
          <Button className="btn btn-primary"> Save </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default AddCoin;
