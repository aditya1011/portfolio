import React from "react";
// import { Typography, Paper, Avatar, Button } from '@material-ui/core'
// import VerifiedUserOutlined from '@material-ui/icons/VerifiedUserOutlined'
// import withStyles from '@material-ui/core/styles/withStyles'
import { withRouter, Link, Redirect } from "react-router-dom";
import firebase from "../firebase";

// const styles = theme => ({
// 	main: {
// 		width: 'auto',
// 		display: 'block', // Fix IE 11 issue.
// 		marginLeft: theme.spacing.unit * 3,
// 		marginRight: theme.spacing.unit * 3,
// 		[theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
// 			width: 400,
// 			marginLeft: 'auto',
// 			marginRight: 'auto',
// 		},
// 	},
// 	paper: {
// 		marginTop: theme.spacing.unit * 8,
// 		display: 'flex',
// 		flexDirection: 'column',
// 		alignItems: 'center',
// 		padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
// 	},
// 	avatar: {
// 		margin: theme.spacing.unit,
// 		backgroundColor: theme.palette.secondary.main,
// 	},
// 	submit: {
// 		marginTop: theme.spacing.unit * 3,
// 	},
// })

function HomePage() {
  return (
    firebase.isLoggedin()?<Redirect to="/dashboard"/>:
    <div className="card">
      <div className="card-body">
        <h5 className="card-title">Hello Guest!</h5>
        <Link to="/register">
          <button
            type="submit"
            fullWidth
            variant="contained"
            color="secondary"
            className="btn btn-primary"
          >
            Register
          </button>
        </Link>
        <Link to="/login">
          <button
            type="submit"
            fullWidth
            variant="contained"
            color="secondary"
            className="btn btn-primary"
          >
            Login
          </button>
        </Link>
      </div>
    </div>
  );
}

export default withRouter(HomePage);
